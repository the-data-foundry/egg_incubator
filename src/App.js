import React, { useState } from "react";
import Interface from "./components/Interface";
import Play from "./components/Play";
import Wrapper from "./components/Wrapper";
import "./styles.css";

export default function App() {
  const [hide, setHide] = useState(false);

  const onClickFunc = () => {
    setHide(true);
  };

  return (
    <div className="App">
      {!hide ? (
        <Wrapper>
          <Play playFunctions={onClickFunc} />
          <Interface stateDays={false} stateHours={false} startPara={false} />
        </Wrapper>
      ) : (
        <Interface stateDays={true} stateHours={true} startPara={true} />
      )}
    </div>
  );
}
