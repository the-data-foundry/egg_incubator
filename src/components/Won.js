import React from "react";
import "./Won.css";

const Won = () => {
  return (
    <div className="wrapper">
      <div className="thumb">🎉</div>
      <div className="textGameOver">Congrats</div>
    </div>
  );
};

export default Won;
