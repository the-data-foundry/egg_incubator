import React from "react";
import Wrapper from "./Wrapper";
import classes from "./Instruction.module.css";

const Instructions = (props) => {
  const onTapFunc = (event) => {
    props.instuctionTapOff(event);
  };
  return (
    <Wrapper className={classes.wrapperInst}>
      <div onClick={onTapFunc} className={classes.instructions}>
        <div className={classes.bgInst}>
          <div className={classes.textAll}>
            <div className={classes.infoInst}>
              A successful egg hatching requires maintenance of the following
              parameters
            </div>
            <div className={classes.titleInst}>Humidity </div>
            <div className={classes.parametersInst}>50 > 20</div>
            <div className={classes.titleInst}>Air</div>
            <div className={classes.parametersInst}>50 > 20</div>
            <div className={classes.titleInst}>Temperature </div>
            <div className={classes.parametersInst}>50 > 20</div>
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export default Instructions;
