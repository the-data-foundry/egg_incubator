import React from "react";
import "./Timer.css";

const Timer = (props) => {
  return (
    <div className="timer">
      <div className="days">{props.days}d</div>
      <div className="hours">{props.hours}hr</div>
    </div>
  );
};

export default Timer;
