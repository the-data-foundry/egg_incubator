import React, { useState } from "react";
import "./Buttons.css";
import Wrapper from "./Wrapper";
import Button from "./Button";
//Images :
import Hum from "../img/Frame@4x.png";
import Air from "../img/Circle_fan@4x.png";
import Temp from "../img/Light@4x.png";
import Spin from "../img/spinner@4x.png";
import Help from "../img/information.png";

const Buttons = (props) => {
  const buttonClickedHandler = (event) => {
    props.buttonAction(event);
  };

  return (
    <Wrapper>
      <Button
        imgLoc={Hum}
        alt="hum"
        class="hum"
        buttonCheck={buttonClickedHandler}
      />
      <Button
        imgLoc={Air}
        alt="air"
        class="air"
        buttonCheck={buttonClickedHandler}
      />
      <Button
        imgLoc={Temp}
        alt="temp"
        class="temp"
        buttonCheck={buttonClickedHandler}
      />
      <Button
        imgLoc={Spin}
        alt="spin"
        class="spin"
        buttonCheck={buttonClickedHandler}
      />
      <Button
        imgLoc={Help}
        alt="help"
        class="help"
        buttonCheck={buttonClickedHandler}
      />
    </Wrapper>
  );
};

export default Buttons;
