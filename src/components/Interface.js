import React, { useState, useEffect } from "react";
import "./Interface.css";
//Dependancies//
import Wrapper from "./Wrapper";
import Background from "./Background";
import Buttons from "./Buttons";
import Numbers from "./Numbers";
import Timer from "./Timer";
import Gameover from "./Gameover";
import Won from "./Won";
import Instructions from "./Instructions";
//Images:
import Incubator from "../img/Incubator@4x.png";
import Bottom from "../img/Bottom@4x.png";
import Egg from "../img/Egg@4x.png";
import Pad from "../img/Pad@4x.png";
import Topui from "../img/Background.png";

const Interface = (props) => {
  //Below are prerequisite
  const [handler, setHandler] = useState(["", props.startPara]);
  const [startDays, setStartDays] = useState(props.stateDays);
  const [startHours, setStartHours] = useState(props.stateHours);

  const [timerDays, setTimerDays] = useState(0);
  const [timerHours, setTimerHours] = useState(0);
  //Below are UI values:
  const [humValue, setHumValue] = useState(45);
  const [humCheck, setHumCheck] = useState(false);
  const [airValue, setAirValue] = useState(40);
  const [airCheck, setAirCheck] = useState(false);
  const [tempValue, setTempValue] = useState(35);
  const [tempCheck, setTempCheck] = useState(false);

  const [spinCheck, setSpinCheck] = useState(false);
  //Game result - Gameover or Congrats:
  const [gameOver, setGameOver] = useState(false);
  const [congrats, setCongrats] = useState(false);

  //Insruction hide;

  const [instHide, setInstHide] = useState(true);

  const handerFunc = (event) => {
    setHandler(event);
    console.log(event);

    //Hum check / Boolion <<< Button
    if (event[0] === "hum" && event[1] === true) {
      setHumCheck(true);
    } else if (event[0] === "hum" && event[1] === false) {
      setHumCheck(false);
    }
    //Air check /Boolion <<< Button
    if (event[0] === "air" && event[1] === true) {
      setAirCheck(true);
    } else if (event[0] === "air" && event[1] === false) {
      setAirCheck(false);
    }

    //Temp check / Boolion <<< Button
    if (event[0] === "temp" && event[1] === true) {
      setTempCheck(true);
    } else if (event[0] === "temp" && event[1] === false) {
      setTempCheck(false);
    }

    //Spin check / Boolion <<< Button
    if (event[0] === "spin" && event[1] === true) {
      setSpinCheck(true);
    } else if (event[0] === "spin" && event[1] === false) {
      setSpinCheck(false);
    }

    if (event[0] === "help" && event[1] === true) {
      setInstHide(false);
    } else if (event[0] === "help" && event[1] === false) {
      setInstHide(true);
    }
  };

  //Humidity controller:

  useEffect(() => {
    if (startDays === true && humCheck === true && humValue <= 74) {
      const id = setInterval(() => {
        setHumValue((humValue) => {
          return humValue + 1;
        });
      }, 2000);
      return () => {
        clearInterval(id);
      };
    } else if (startDays === true && humCheck === false && humValue >= 1) {
      const id = setInterval(() => {
        setHumValue((humValue) => {
          return humValue - 1;
        });
      }, 2000);
      return () => {
        clearInterval(id);
      };
    }
  }, [startDays, humCheck, humValue, setHumValue]);

  // Air controller:
  useEffect(() => {
    if (startDays === true && airCheck === true && airValue <= 74) {
      const id = setInterval(() => {
        setAirValue((airValue) => {
          return airValue + 1;
        });
      }, 1000);
      return () => {
        clearInterval(id);
      };
    } else if (startDays === true && airCheck === false && airValue >= 1) {
      const id = setInterval(() => {
        setAirValue((airValue) => {
          return airValue - 1;
        });
      }, 2000);
      return () => {
        clearInterval(id);
      };
    }
  }, [startDays, airCheck, airValue, setAirValue]);

  // Temp controller:
  useEffect(() => {
    if (startDays === true && tempCheck === true && tempValue <= 74) {
      const id = setInterval(() => {
        setTempValue((tempValue) => {
          return tempValue + 1;
        });
      }, 1000);
      return () => {
        clearInterval(id);
      };
    } else if (startDays === true && tempCheck === false && tempValue >= 1) {
      const id = setInterval(() => {
        setTempValue((tempValue) => {
          return tempValue - 1;
        });
      }, 1000);
      return () => {
        clearInterval(id);
      };
    }
  }, [startDays, tempCheck, tempValue, setTempValue]);

  //Timer controller:
  useEffect(() => {
    if (startDays && startHours && timerDays <= 23) {
      const idTimerDays = setInterval(() => {
        setTimerDays((Days) => Days + 0.1);
        setTimerHours((hours) => {
          if (hours <= 23) {
            return hours + 1;
          } else if (hours === 24) {
            return (hours = 0);
          }
        });
      }, 500);
      return () => {
        clearInterval(idTimerDays);
      };
    } else {
      setTimerDays("23");
      setTimerHours("0");
    }
  }, [startDays, startHours, setStartDays, setStartHours, timerDays]);

  //GameOVer function:
  useEffect(() => {
    if (humValue < 40 || humValue > 50) {
      setGameOver(true);
    } else if (airValue < 30 || airValue > 50) {
      setGameOver(true);
    } else if (tempValue < 30 || tempValue > 40) {
      setGameOver(true);
    } else if (spinCheck === false && timerDays >= 22 && startDays === true) {
      setGameOver(true);
    }
  }, [startDays, timerDays, humValue, airValue, tempValue, spinCheck]);

  // Congrats page:
  useEffect(() => {
    if (gameOver === false && timerDays === 23) {
      setCongrats(true);
    }
  }, [gameOver, timerDays, setCongrats]);

  // Help button:

  // Instruction slider
  const instructionHide = () => {
    if (instHide) {
      setInstHide(false);
    } else {
      setInstHide(true);
    }
  };

  return (
    <Wrapper className="wrapper">
      {!instHide && <Instructions instuctionTapOff={instructionHide} />}
      {gameOver && <Gameover />}
      {congrats && <Won />}

      <Background imgLoc={Incubator} alt="background" class="incubator" />
      <Background
        imgLoc={Egg}
        alt="egg"
        class={spinCheck ? "eggSpin" : "egg"}
      />
      <Background imgLoc={Bottom} alt="bottom" class="bottom" />
      <Background imgLoc={Pad} alt="pad" class="pad" />
      <Background imgLoc={Topui} alt="topui" class="topui" />

      <Buttons buttonAction={handerFunc} />

      <div className="ui">
        <Numbers
          className="hum"
          title="Hum: "
          value={`${parseFloat(humValue)}%`}
          colorStyle={humValue === 75 || humValue === 0 ? "orange" : "gray"}
        />
        <Numbers
          className="air"
          title="Air: "
          value={airValue}
          colorStyle={airValue === 75 || airValue === 0 ? "orange" : "gray"}
        />
        <Numbers
          className="temp"
          title="Temp: "
          value={`${tempValue}°`}
          colorStyle={tempValue === 75 || tempValue === 0 ? "orange" : "gray"}
        />
      </div>

      <Timer days={Math.round(timerDays)} hours={timerHours} />
    </Wrapper>
  );
};

export default Interface;
