import React from "react";
import "./Gameover.css";

const Gameover = () => {
  return (
    <div className="wrapper">
      <div className="thumb">👎</div>
      <div className="textGameOver">Game Over</div>
    </div>
  );
};

export default Gameover;
