import React from "react";
import Playimg from "../img/play.png";
import "./Play.css";

const Play = (props) => {
  const playHandlerFunc = () => {
    props.playFunctions();
  };

  return (
    <div className="wrapper" onClick={playHandlerFunc}>
      <img src={Playimg} alt="play_image" className="playLogo" />
      <div className="description">Click here</div>
    </div>
  );
};

export default Play;
