import React, { useState } from "react";
import Wrapper from "./Wrapper";
import "./Button.css";

const Button = (props) => {
  const [toggle, setToggle] = useState(true);

  const onClickListener = (event) => {
    props.buttonCheck([props.class, toggle]);

    setToggle(() => {
      if (toggle === false) {
        return true;
      } else if (toggle === true) {
        return false;
      }
    });
  };

  return (
    <Wrapper className="button">
      <img
        src={props.imgLoc}
        alt={props.alt}
        className={props.class}
        onClick={onClickListener}
        value={props.value}
        style={toggle ? { opacity: 0.3 } : { opacity: 1 }}
      />
    </Wrapper>
  );
};

export default Button;
