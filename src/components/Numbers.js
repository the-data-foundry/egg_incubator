import React from "react";
import "./Numbers.css";

const Numbers = (props) => {
  return (
    <div className="number">
      <span className="title">{props.title}</span>
      <span className="value" style={{ color: props.colorStyle }}>
        {props.value}
      </span>
    </div>
  );
};

export default Numbers;
