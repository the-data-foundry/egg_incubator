import React from "react";
import "./Background.css";

const Background = (props) => {
  return (
    <img
      src={props.imgLoc}
      alt={props.alt}
      width={null}
      height={null}
      className={props.class}
    />
  );
};

export default Background;
